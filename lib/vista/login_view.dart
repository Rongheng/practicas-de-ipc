import 'package:flutter/material.dart';
import 'products_list_view.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Inicio de Sesión"),
        backgroundColor: const Color.fromARGB(255, 26, 142, 5),
        actions: [
          CircleAvatar(
            radius: 5,
            backgroundColor: const Color.fromARGB(197, 68, 7, 209),
            child: Icon(Icons.circle, size: 100),
          ),
          SizedBox(width: 5),
          CircleAvatar(
            radius: 5,
            backgroundColor: const Color.fromARGB(197, 68, 7, 209),
            child: Icon(Icons.circle, size: 5),
          ),
          SizedBox(width: 50),
          CircleAvatar(
            radius: 5,
            backgroundColor: const Color.fromARGB(197, 68, 7, 209),
            child: Icon(Icons.circle, size: 50),
          ),
          SizedBox(width: 5),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 300,
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(
                      color: const Color.fromARGB(255, 23, 215, 129)),
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Center(
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Nombre de usuario',
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 16),
            SizedBox(
              width: 300,
              child: Container(
                alignment: const Alignment(1, 1),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: const Color.fromARGB(255, 18, 227, 193)),
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Center(
                  child: TextFormField(
                    obscureText: true,
                    decoration: const InputDecoration(
                      labelText: 'Contraseña',
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                print('Iniciar sesión');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ProductsListView()),
                );
              },
              child: const Text('Iniciar Sesión'),
            ),
          ],
        ),
      ),
    );
  }
}

void main() {
  runApp(const MaterialApp(
    home: LoginView(),
  ));
}
